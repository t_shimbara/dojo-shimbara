package dojo.exam3;

public class Exam3 {

	
	/* 使用してほしいテクニック
	 * 階層の抽出
	 * 
	 * オブジェクトによる配列の置き換え
	 * 重複した条件記述の断片の統合
	 * メソッドの抽出、移動
	 * ポリモーフィズムによる条件記述の置き換え（必須ではない)
	 */
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] reportHeader = new String[] {
				"date", "type", "price", "processFlg"
		};
		
		String[][] csvData = new String[][] {
				{"2013/01/01", "10", "20000", "1"},
				{"2013/01/01", "20", "20000", "0"},
				{"2013/01/01", "10", "20000", "0"},
				{"2013/01/01", "20", "35000", "1"},
				{"2013/01/01", "20", "40000", "1"}
		};
		
		Report report = new Report(reportHeader, csvData);
		
		System.out.println(report.getReportTitle()+":レポート");
		for(String header : report.getReportHeaderNames()) {
			System.out.print(header);
		}
		System.out.println("");
		System.out.println("非課税合計"+report.getSubTotal10());
		System.out.println("課税合計"+report.getSubTotal20());
		System.out.println("税金合計"+report.getTaxTotal());
		System.out.println("合計"+report.getTotal());
		
		if ("pdf".equals(args[0])) {
			Pdf pdf = new Pdf();
			pdf.setTitle(report.getReportTitle()+":レポート");
			pdf.setTaxValue(report.getTaxTotal());
			pdf.setTotalValue(report.getTotal());
			pdf.saveAsFile();
		}
		else if ("db".equals(args[0])) {
			Database database = new Database();
			database.insert(report.getSubTotal10(), report.getSubTotal20(), report.getTaxTotal(), report.getTotal());
		}
	}

}
