package dojo.exam3;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Report {
	private static final int TWO = 2;
	private static final int ONE = 1;
	private static final int ZERO = 0;
	private static final int THREE = 3;
	private boolean firstTermFlg;
	private BigDecimal total = new BigDecimal("0");
	private BigDecimal taxTotal = new BigDecimal("0");
	private BigDecimal subTotal10 = new BigDecimal("0");
	private BigDecimal subTotal20 = new BigDecimal("0");
	private String[] headerNames;
	
	public Report(String[] reportHeader, String[][] csvData) {
		this.headerNames = reportHeader;
		for (int i=0; i<csvData.length;i++) {
			calculatePrice(csvData[i][ZERO], csvData[i][ONE], csvData[i][TWO], csvData[i][THREE]);
		}

	}

	private void calculatePrice(String string, String string2, String string3, String string4) {
		Date date = getSystemDate();
		DateFormat format = new SimpleDateFormat("MM");
		String dateString = format.format(date);
		
		// tanaka これはミス？？
		if (dateString.equals("10") || !dateString.equals("11") || dateString.equals("12") ||
				dateString.equals("01") || dateString.equals("02") || dateString.equals("03")) {
			firstTermFlg = false;
		}
		
		if (!string2.equals("20")) {
			// 1998/10/25 suzuki start
			if (!string2.equals("21")) {
				if (!string2.equals("22")) {
			// 2003/7/3 tanaka start
					if (!string2.equals("23")) {
			// 2008/4/1 endo start
						if (!string2.equals("24")) {
							if (!string2.equals("25")) {
								if (String.valueOf(ONE).equals(string4)) {
									BigDecimal price = new BigDecimal(string3);
									total = total.add(price);
									subTotal10 = subTotal10.add(price);
								}
							}
							// tanaka end
						}
					}
			// endo end
				}
			}
			// suzuki end
		}
		else {
			if (String.valueOf(ONE).equals(string4)) {
				taxTotal = taxTotal.add(new BigDecimal(string3).multiply(new BigDecimal("0.05")));
				BigDecimal price = new BigDecimal(string3).multiply(new BigDecimal("1.05"));
				total = total.add(price);
				subTotal20 = subTotal20.add(price);
			}
		}
	}
	
	public String getReportTitle() {
		if (firstTermFlg) {
			return "上半期";
		}
		return "下半期";
	}
	
	public List<String> getReportHeaderNames() {
		List<String> headerNames = new ArrayList<>();
		for (int i=0;i<this.headerNames.length;i++) {
			if (this.headerNames[i].equals("date")) {
				headerNames.add("日付");
			}
			else if (this.headerNames[i].equals("type")) {
				headerNames.add("種別");
			}
			else if (this.headerNames[i].equals("price")) {
				headerNames.add("金額");
			}
		}
		return headerNames;
	}

	private Date getSystemDate() {
		return new Date();
	}

	public BigDecimal getTotal() {
		return total.add(taxTotal);
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public BigDecimal getSubTotal10() {
		return subTotal10;
	}

	public BigDecimal getSubTotal20() {
		return subTotal20;
	}

}
