package dojo.exam2;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@SuppressWarnings("serial")
public class LoginFrame extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Create the frame.
	 */
	public LoginFrame() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(138, 97, 64, 13);
		contentPane.add(lblNewLabel);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(138, 122, 64, 13);
		contentPane.add(lblPassword);
		
		textField = new JTextField();
		textField.setBounds(220, 95, 100, 17);
		contentPane.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(220, 120, 100, 17);
		contentPane.add(passwordField);
		
		final JLabel messageLabel = new JLabel("");
		messageLabel.setForeground(Color.RED);
		messageLabel.setBounds(138, 70, 182, 13);
		contentPane.add(messageLabel);
		
		KeyAdapter enterKeyAdapter = new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (KeyEvent.VK_ENTER == e.getKeyCode()) {
					messageLabel.setText("");
					
					if ("".equals(textField.getText())) {
						messageLabel.setText("Name を入力してください");
						return;
					}
					
					if ("".equals(String.valueOf(passwordField.getPassword()))){
						messageLabel.setText("Pasword を入力してください");
						return;
					}
					
					
					Connection connection = null;
					PreparedStatement stmt = null;
					ResultSet rset = null;
					try {
						connection = DriverManager.getConnection("jdbc:derby:memory:patient;create=true");
						stmt = connection.prepareStatement("SELECT id, name FROM USERS WHERE name = ? and password = ?");
						stmt.setString(1, textField.getText());
						stmt.setString(2, String.valueOf(passwordField.getPassword()));
						rset = stmt.executeQuery();
						
						if (!rset.next()) {
							EventQueue.invokeLater(new Runnable() {
								public void run() {
									try {
										messageLabel.setText("ログインに失敗しました");
									}
									catch(Exception e) {
										e.printStackTrace();
									}
								}
							});
						}
						else {
							EventQueue.invokeLater(new Runnable() {
								public void run() {
									dispose();
									ListFrame frame = new ListFrame();
									frame.setVisible(true);
								}
							});
						}
					} catch (SQLException e1) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									messageLabel.setText("データベース異常が発生しました");
								}
								catch(Exception e) {								
								}
							}
						});
					}
					finally {
						if (rset != null) {
							try {
								rset.close();
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
						}
						if (stmt != null) {
							try {
								stmt.close();
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
						}
						if (connection != null) {
							try {
								connection.close();
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
						}
					}
				}
			}
		};
		
		textField.addKeyListener(enterKeyAdapter);
		passwordField.addKeyListener(enterKeyAdapter);
		
		JButton btnLogin = new JButton("Login");		
		btnLogin.addKeyListener(enterKeyAdapter);
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				messageLabel.setText("");
				
				Connection connection = null;
				PreparedStatement stmt = null;
				ResultSet rset = null;
				try {
					connection = DriverManager.getConnection("jdbc:derby:memory:patient;create=true");
					stmt = connection.prepareStatement("SELECT id, name FROM USERS WHERE name = ? and password = ?");
					stmt.setString(1, textField.getText());
					stmt.setString(2, String.valueOf(passwordField.getPassword()));
					rset = stmt.executeQuery();
					
					if (!rset.next()) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									messageLabel.setText("ログインに失敗しました");
								}
								catch(Exception e) {
									e.printStackTrace();
								}
							}
						});
					}
					else {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								dispose();
								ListFrame frame = new ListFrame();
								frame.setVisible(true);
							}
						});
					}
				} catch (SQLException e1) {
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							try {
								messageLabel.setText("データベース異常が発生しました");
							}
							catch(Exception e) {								
							}
						}
					});
				}
				finally {
					if (rset != null) {
						try {
							rset.close();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					if (stmt != null) {
						try {
							stmt.close();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					if (connection != null) {
						try {
							connection.close();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
		btnLogin.setBounds(212, 156, 108, 23);
		contentPane.add(btnLogin);
	}
}
