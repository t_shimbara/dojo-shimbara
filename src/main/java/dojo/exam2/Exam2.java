package dojo.exam2;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Exam2 {
	
	/* 使用してほしいテクニック
	 * プレゼンテーションとドメインの分離
	 * 
	 * クラスの抽出
	 */

	/**
	 * Launch the application.
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {		
		//このデータ初期投入処理は、リファクタリング対象外です。	>>>>>>>>>>>>>
		Connection connection = DriverManager.getConnection("jdbc:derby:memory:patient;create=true");
		Statement stmt = null;
		
		try {
			stmt = connection.createStatement();
			stmt.execute("CREATE TABLE USERS (id int primary key generated always as identity, name varchar(100), password varchar(100))");
			stmt.execute("INSERT INTO USERS(name, password) VALUES('sample', 'sample')");
			stmt.execute("INSERT INTO USERS(name, password) VALUES('foo', 'bar')");
		}
		finally {
			if (stmt != null) {
				stmt.close();	
			}
		}
		// ここまで >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
						
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFrame loginFrame = new LoginFrame();
					loginFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
