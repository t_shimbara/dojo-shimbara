package dojo.exam2;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class ListFrame extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	private JLabel lblNewLabel;
	private JPasswordField passwordField;
	private JLabel lblMessage;


	/**
	 * Create the frame.
	 */
	public ListFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		int count = 0;
		String[][] values = null;
		Connection connection = null;
		PreparedStatement stmt = null;
		ResultSet rset = null;
		try {
			connection = DriverManager.getConnection("jdbc:derby:memory:patient;create=true");
			stmt = connection.prepareStatement("SELECT NAME, PASSWORD FROM USERS");
			rset = stmt.executeQuery();
			List<String> names = new ArrayList<>();
			while(rset.next()) {
				names.add(rset.getString(1));
			}
			values = new String[names.size()][1];
			for (int i=0;i<values.length;i++) {
				values[i][0] = names.get(i);
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		finally {
			if (rset != null) {
				try {
					rset.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		DefaultTableModel tableModel = new DefaultTableModel(values, new String[]{"Name"}) {
			public boolean isCellEditable(int row, int column) {
				return false;
			};			
		};

		table = new JTable(tableModel);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(12, 98, 424, 161);
		contentPane.add(scrollPane);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(12, 30, 64, 13);
		contentPane.add(lblName);
		
		
		textField = new JTextField();
		textField.setBounds(58, 28, 104, 17);
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblNewLabel = new JLabel("Password");
		lblNewLabel.setBounds(180, 30, 64, 13);
		contentPane.add(lblNewLabel);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(249, 29, 108, 15);
		contentPane.add(passwordField);
		
		
		lblMessage = new JLabel("");
		lblMessage.setForeground(Color.RED);
		lblMessage.setBounds(12, 5, 271, 13);
		contentPane.add(lblMessage);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblMessage.setText("");
				
				//validation
				if ("".equals(textField.getText())) {
					lblMessage.setText("Name を入力してください");
					return;
				}
				
				if ("".equals(String.valueOf(passwordField.getPassword()))){
					lblMessage.setText("password を入力してください");
					return;
				}
				
				if (textField.getText().length() < 3) {
					lblMessage.setText("Name は3文字以上とする必要があります");
					return;
				}
				
				if (String.valueOf(passwordField.getPassword()).length() < 3){
					lblMessage.setText("password は3文字以上とする必要があります");
					return;
				}
				
				for (int i=0;i<table.getModel().getRowCount();i++) {
					if (textField.getText().equals(table.getModel().getValueAt(i, 0).toString())) {
						lblMessage.setText("すでに同一の Name が使用されています");
						return;
					}
				}
				
				//insert
				Connection connection = null;
				PreparedStatement stmt = null;
				ResultSet rset = null;
				try {
					connection = DriverManager.getConnection("jdbc:derby:memory:patient;create=true");
					stmt = connection.prepareStatement("INSERT INTO USERS (name, password) VALUES (?,?)");
					stmt.setString(1, textField.getText());
					stmt.setString(2, String.valueOf(passwordField.getPassword()));
					int updateRowNumber = stmt.executeUpdate();
					
					stmt.close();
					stmt = connection.prepareStatement("SELECT NAME, PASSWORD FROM USERS");
					rset = stmt.executeQuery();
					EventQueue.invokeLater(new Runnable() {
						public void run() {
							((DefaultTableModel)table.getModel()).setRowCount(0);
						}
					});
					while(rset.next()) {
						final String value = rset.getString(1);
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								((DefaultTableModel)table.getModel()).addRow(new String[]{value});
							}
						});
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				finally {
					if (rset != null) {
						try {
							rset.close();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					if (stmt != null) {
						try {
							stmt.close();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
					if (connection != null) {
						try {
							connection.close();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
					}
				}
				
			}
		});
		btnAdd.setBounds(328, 63, 108, 23);
		contentPane.add(btnAdd);

		
	}
}
