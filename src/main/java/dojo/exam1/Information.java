package dojo.exam1;

import dojo.exam1.BasicPostage.Area;

public class Information {

  private int width;
  private int height;
  private int depth;
  private Area area;

  public int getWidth() {
    return width;
  }
  public void setWidth(int width) {
    this.width = width;
  }
  public int getHeight() {
    return height;
  }
  public void setHeight(int height) {
    this.height = height;
  }
  public int getDepth() {
    return depth;
  }
  public void setDepth(int depth) {
    this.depth = depth;
  }
  public Area getArea() {
    return area;
  }
  public void setArea(Area area) {
    this.area = area;
  }

}
