package dojo.exam1;

import dojo.exam1.BasicPostage.Area;

public class Exam1 {

  public static void main(String[] args) {
    Information information = new Information();
    information.setWidth(10);
    information.setHeight(10);
    information.setDepth(30);
    information.setArea(Area.AREA_1);

    System.out.println(BasicPostage.calc(information));
  }
}
