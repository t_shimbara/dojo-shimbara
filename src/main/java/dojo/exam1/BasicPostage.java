package dojo.exam1;

import java.util.*;

public class BasicPostage {
  public enum Area {
    AREA_0,
    AREA_1,
    AREA_2,
    AREA_3,
    AREA_4,
    AREA_5,
    AREA_6,
    AREA_7
  };

  private enum Size {
    SIZE_60(60),
    SIZE_80(80),
    SIZE_100(100),
    SIZE_120(120),
    SIZE_140(140),
    SIZE_160(160),
    SIZE_170(170);

    private final int threshold;
    Size(int threshold) {
      this.threshold = threshold;
    }

    static Size judge(Information information) {
      int sum = information.getWidth() + information.getHeight() + information.getDepth();
      for (Size size : Size.values()) {
        if (sum <= size.threshold) {
          return size;
        }
      }
      throw new IllegalArgumentException();
    }
  }

  private static class PostageTable {
    private static final Map<Area,Map<Size,Integer>> table;

    static {
      final Area[] areas = Area.values();
      final Size[] sizes = Size.values();
      final int areaLength = areas.length;
      final int sizeLength = sizes.length;
      final int[][] postage = new int[][] {
          // 県内     610円   810円 1,030円 1,230円 1,440円 1,650円 1,750円
          // 第1地帯   710円   930円 1,130円 1,340円 1,540円 1,750円 1,950円
          // 第2地帯   810円 1,030円 1,230円 1,440円 1,650円 1,850円 2,060円
          // 第3地帯   930円 1,130円 1,340円 1,540円 1,750円 1,950円 2,160円
          // 第4地帯 1,030円 1,230円 1,440円 1,650円 1,850円 2,060円 2,260円
          // 第5地帯 1,130円 1,340円 1,540円 1,750円 1,950円 2,160円 2,370円
          // 第6地帯 1,230円 1,440円 1,650円 1,850円 2,060円 2,260円 2,470円
          // 第7地帯 1,340円 1,540円 1,750円 1,950円 2,160円 2,370円 2,570円

          /*          SIZE_60,   80,  100,  120,  140,  160,  170 */
          /* AREA_0 */ {  610,  810, 1030, 1230, 1440, 1650, 1750},
          /* AREA_1 */ {  710,  930, 1130, 1340, 1540, 1750, 1950},
          /* AREA_2 */ {  810, 1030, 1230, 1440, 1650, 1850, 2060},
          /* AREA_3 */ {  930, 1130, 1340, 1540, 1750, 1950, 2160},
          /* AREA_4 */ { 1030, 1230, 1440, 1650, 1850, 2060, 2260},
          /* AREA_5 */ { 1130, 1340, 1540, 1750, 1950, 2160, 2370},
          /* AREA_6 */ { 1230, 1440, 1650, 1850, 2060, 2260, 2470},
          /* AREA_7 */ { 1340, 1540, 1750, 1950, 2160, 2370, 2570}
      };

      Map<Area,Map<Size,Integer>> tmpTable = new HashMap<>(areaLength);
      for (int i = 0; i < areaLength; i++) {
        Map<Size,Integer> row = new HashMap<>(sizeLength);
        for (int j = 0; j < sizeLength; j++) {
          row.put(sizes[j], Integer.valueOf(postage[i][j]));
        }
        tmpTable.put(areas[i], Collections.unmodifiableMap(row));
      }

      table = Collections.unmodifiableMap(tmpTable);
    }

    static Integer get(Area area, Size size) {
      return table.get(area).get(size);
    }
  }

  public static int calc(Information information) {
    return PostageTable.get(information.getArea(), Size.judge(information));
  }
}
