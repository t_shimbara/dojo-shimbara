package dojo.exam1;

import dojo.exam1.BasicPostage.Area;

import org.junit.runner.RunWith;
import org.junit.experimental.runners.Enclosed;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theory;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(Enclosed.class)
public class Exam1Test {

  static void 運賃が正しいか(TestData testData) {
    Information information = new Information();
    information.setWidth(testData.width);
    information.setHeight(testData.height);
    information.setDepth(testData.depth);
    information.setArea(testData.area);

    assertThat(testData.reason(), BasicPostage.calc(information), is(testData.expectedFee));
  }

  static class TestData {
    Area area;
    int width;
    int height;
    int depth;
    int expectedFee;

    TestData(Area area, int width, int height, int depth, int expectedFee) {
      this.area = area;
      this.width = width;
      this.height = height;
      this.depth = depth;
      this.expectedFee = expectedFee;
    }

    public String reason() {
      return String.format(
          "配送区域:%s, サイズ:(%d,%d,%d), 運賃の期待値:%d",
          this.area,
          this.width,
          this.height,
          this.depth,
          this.expectedFee);
    }
  }
  @RunWith(Theories.class)
  public static class 運賃_県内 {
    @DataPoints
    public static TestData[] testDatas = {
        // 県内	610円	810円	1,030円	1,230円	1,440円	1,650円	1,750円
        new TestData(Area.AREA_0, 20, 20,  20,  610),  // 60サイズ
        new TestData(Area.AREA_0, 20, 20,  40,  810),  // 80サイズ
        new TestData(Area.AREA_0, 20, 20,  60, 1030),  // 100サイズ
        new TestData(Area.AREA_0, 20, 20,  80, 1230),  // 120サイズ
        new TestData(Area.AREA_0, 20, 20, 100, 1440),  // 140サイズ
        new TestData(Area.AREA_0, 20, 20, 120, 1650),  // 160サイズ
        new TestData(Area.AREA_0, 20, 20, 130, 1750)   // 170サイズ
    };

    @Theory
    public void test(TestData testData) {
      運賃が正しいか(testData);
    }
  }

  @RunWith(Theories.class)
  public static class 運賃_第1地帯 {
    @DataPoints
    public static TestData[] testDatas = {
        // 第1地帯	710円	930円	1,130円	1,340円	1,540円	1,750円	1,950円
        new TestData(Area.AREA_1, 20, 20,  20,  710),  // 60サイズ
        new TestData(Area.AREA_1, 20, 20,  40,  930),  // 80サイズ
        new TestData(Area.AREA_1, 20, 20,  60, 1130),  // 100サイズ
        new TestData(Area.AREA_1, 20, 20,  80, 1340),  // 120サイズ
        new TestData(Area.AREA_1, 20, 20, 100, 1540),  // 140サイズ
        new TestData(Area.AREA_1, 20, 20, 120, 1750),  // 160サイズ
        new TestData(Area.AREA_1, 20, 20, 130, 1950)   // 170サイズ
    };

    @Theory
    public void test(TestData testData) {
      運賃が正しいか(testData);
    }
  }

  @RunWith(Theories.class)
  public static class 運賃_第2地帯 {
    @DataPoints
    public static TestData[] testDatas = {
        // 第2地帯	810円	1,030円	1,230円	1,440円	1,650円	1,850円	2,060円
        new TestData(Area.AREA_2, 20, 20,  20,  810),  // 60サイズ
        new TestData(Area.AREA_2, 20, 20,  40, 1030),  // 80サイズ
        new TestData(Area.AREA_2, 20, 20,  60, 1230),  // 100サイズ
        new TestData(Area.AREA_2, 20, 20,  80, 1440),  // 120サイズ
        new TestData(Area.AREA_2, 20, 20, 100, 1650),  // 140サイズ
        new TestData(Area.AREA_2, 20, 20, 120, 1850),  // 160サイズ
        new TestData(Area.AREA_2, 20, 20, 130, 2060)   // 170サイズ
    };

    @Theory
    public void test(TestData testData) {
      運賃が正しいか(testData);
    }
  }

  @RunWith(Theories.class)
  public static class 運賃_第3地帯 {
    @DataPoints
    public static TestData[] testDatas = {
        // 第3地帯	930円	1,130円	1,340円	1,540円	1,750円	1,950円	2,160円
        new TestData(Area.AREA_3, 20, 20,  20,  930),  // 60サイズ
        new TestData(Area.AREA_3, 20, 20,  40, 1130),  // 80サイズ
        new TestData(Area.AREA_3, 20, 20,  60, 1340),  // 100サイズ
        new TestData(Area.AREA_3, 20, 20,  80, 1540),  // 120サイズ
        new TestData(Area.AREA_3, 20, 20, 100, 1750),  // 140サイズ
        new TestData(Area.AREA_3, 20, 20, 120, 1950),  // 160サイズ
        new TestData(Area.AREA_3, 20, 20, 130, 2160)   // 170サイズ
    };

    @Theory
    public void test(TestData testData) {
      運賃が正しいか(testData);
    }
  }

  @RunWith(Theories.class)
  public static class 運賃_第4地帯 {
    @DataPoints
    public static TestData[] testDatas = {
        // 第4地帯	1,030円	1,230円	1,440円	1,650円	1,850円	2,060円	2,260円
        new TestData(Area.AREA_4, 20, 20,  20, 1030),  // 60サイズ
        new TestData(Area.AREA_4, 20, 20,  40, 1230),  // 80サイズ
        new TestData(Area.AREA_4, 20, 20,  60, 1440),  // 100サイズ
        new TestData(Area.AREA_4, 20, 20,  80, 1650),  // 120サイズ
        new TestData(Area.AREA_4, 20, 20, 100, 1850),  // 140サイズ
        new TestData(Area.AREA_4, 20, 20, 120, 2060),  // 160サイズ
        new TestData(Area.AREA_4, 20, 20, 130, 2260)   // 170サイズ
    };

    @Theory
    public void test(TestData testData) {
      運賃が正しいか(testData);
    }
  }

  @RunWith(Theories.class)
  public static class 運賃_第5地帯 {
    @DataPoints
    public static TestData[] testDatas = {
        // 第5地帯	1,130円	1,340円	1,540円	1,750円	1,950円	2,160円	2,370円
        new TestData(Area.AREA_5, 20, 20,  20, 1130),  // 60サイズ
        new TestData(Area.AREA_5, 20, 20,  40, 1340),  // 80サイズ
        new TestData(Area.AREA_5, 20, 20,  60, 1540),  // 100サイズ
        new TestData(Area.AREA_5, 20, 20,  80, 1750),  // 120サイズ
        new TestData(Area.AREA_5, 20, 20, 100, 1950),  // 140サイズ
        new TestData(Area.AREA_5, 20, 20, 120, 2160),  // 160サイズ
        new TestData(Area.AREA_5, 20, 20, 130, 2370)   // 170サイズ
    };

    @Theory
    public void test(TestData testData) {
      運賃が正しいか(testData);
    }
  }

  @RunWith(Theories.class)
  public static class 運賃_第6地帯 {
    @DataPoints
    public static TestData[] testDatas = {
        // 第6地帯	1,230円	1,440円	1,650円	1,850円	2,060円	2,260円	2,470円
        new TestData(Area.AREA_6, 20, 20,  20, 1230),  // 60サイズ
        new TestData(Area.AREA_6, 20, 20,  40, 1440),  // 80サイズ
        new TestData(Area.AREA_6, 20, 20,  60, 1650),  // 100サイズ
        new TestData(Area.AREA_6, 20, 20,  80, 1850),  // 120サイズ
        new TestData(Area.AREA_6, 20, 20, 100, 2060),  // 140サイズ
        new TestData(Area.AREA_6, 20, 20, 120, 2260),  // 160サイズ
        new TestData(Area.AREA_6, 20, 20, 130, 2470)   // 170サイズ
    };

    @Theory
    public void test(TestData testData) {
      運賃が正しいか(testData);
    }
  }

  @RunWith(Theories.class)
  public static class 運賃_第7地帯 {
    @DataPoints
    public static TestData[] testDatas = {
        // 第7地帯	1,340円	1,540円	1,750円	1,950円	2,160円	2,370円	2,570円
        new TestData(Area.AREA_7, 20, 20,  20, 1340),  // 60サイズ
        new TestData(Area.AREA_7, 20, 20,  40, 1540),  // 80サイズ
        new TestData(Area.AREA_7, 20, 20,  60, 1750),  // 100サイズ
        new TestData(Area.AREA_7, 20, 20,  80, 1950),  // 120サイズ
        new TestData(Area.AREA_7, 20, 20, 100, 2160),  // 140サイズ
        new TestData(Area.AREA_7, 20, 20, 120, 2370),  // 160サイズ
        new TestData(Area.AREA_7, 20, 20, 130, 2570)   // 170サイズ
    };

    @Theory
    public void test(TestData testData) {
      運賃が正しいか(testData);
    }
  }
}
